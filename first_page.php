<!DOCTYPE html>
<html>

<head>
<title>SDM Homework #2 - First Page</title>
<link rel="stylesheet" type="text/css" href="style.css" >

<!-- Script for query processing -->

<script src="jquery-2.1.4.js"></script>

<script>
$( document ).ready(
	function()
	{
		$( "#formsearch" ).click
		(
			function()
			{
				var tar = document.getElementById( "mainform" ).elements.namedItem( "user" ).value;

				$.ajax ( { url: "ajax_page.php", data: "user=" + tar, 
					success: function( ret ) { document.getElementById( "result" ).innerHTML = ret; } } );
				return false;
			}
		);
	}
);
</script>

</head>

<body>

<!-- Query form -->

<center>
<form method="post" align="center" id="mainform">
<input type="text" name="user">
<input type="submit" value="search" id="formsearch">
</form>
<br>
</center>

<!-- Result Table -->

<table align=center style="width:50%" id="result">
<tr><th colspan=2>Orders</th></tr>
<tr><th style="width:40%;">ID</th><th>Customer</th></tr>
<tr><td colspan=2>No Order</td></tr>
</table>

<!-- End of page body -->

</body>

</html>
