-- MySQL dump 10.13  Distrib 5.5.44, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: order_application
-- ------------------------------------------------------
-- Server version	5.5.44-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer` (
  `UserID` varchar(20) NOT NULL,
  `Name` varchar(20) NOT NULL,
  PRIMARY KEY (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer`
--

LOCK TABLES `customer` WRITE;
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
INSERT INTO `customer` VALUES ('1','Alice'),('2','Bob'),('3','Chris'),('4','Diana'),('5','Eva'),('6','Frank');
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item`
--

DROP TABLE IF EXISTS `item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item` (
  `ItemID` varchar(20) NOT NULL,
  `ItemName` varchar(20) NOT NULL,
  PRIMARY KEY (`ItemID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item`
--

LOCK TABLES `item` WRITE;
/*!40000 ALTER TABLE `item` DISABLE KEYS */;
INSERT INTO `item` VALUES ('1','Apple'),('2','Banana'),('3','Cherry'),('4','Date'),('5','Elderberry'),('6','Fig'),('7','Grape');
/*!40000 ALTER TABLE `item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `OrderID` varchar(20) NOT NULL,
  `CustomerID` varchar(20) NOT NULL,
  PRIMARY KEY (`OrderID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES ('1','3'),('10','6'),('11','3'),('12','3'),('13','4'),('14','3'),('15','4'),('16','3'),('17','3'),('18','3'),('19','2'),('2','4'),('20','6'),('21','3'),('22','6'),('23','6'),('24','6'),('25','1'),('26','5'),('27','4'),('28','1'),('29','2'),('3','6'),('30','6'),('4','3'),('5','3'),('6','3'),('7','6'),('8','4'),('9','5');
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `report`
--

DROP TABLE IF EXISTS `report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `report` (
  `OrderID` varchar(20) NOT NULL,
  `ItemID` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `report`
--

LOCK TABLES `report` WRITE;
/*!40000 ALTER TABLE `report` DISABLE KEYS */;
INSERT INTO `report` VALUES ('1','1'),('1','4'),('1','5'),('1','6'),('1','7'),('2','5'),('2','7'),('3','4'),('3','6'),('4','2'),('4','3'),('4','5'),('5','2'),('5','5'),('5','6'),('5','7'),('6','3'),('6','4'),('6','7'),('7','1'),('7','2'),('8','3'),('8','7'),('9','1'),('9','4'),('9','5'),('9','6'),('10','1'),('10','2'),('10','3'),('10','4'),('10','5'),('10','6'),('10','7'),('11','2'),('11','5'),('11','7'),('12','1'),('12','2'),('12','6'),('12','7'),('13','2'),('13','5'),('13','6'),('13','7'),('14','2'),('15','2'),('15','3'),('16','2'),('16','4'),('16','5'),('16','6'),('16','7'),('17','1'),('17','3'),('17','5'),('17','7'),('18','1'),('18','4'),('18','5'),('19','3'),('19','4'),('20','5'),('20','6'),('21','1'),('21','2'),('21','3'),('21','4'),('21','5'),('21','7'),('22','2'),('22','3'),('22','4'),('22','5'),('23','2'),('23','4'),('23','7'),('24','2'),('24','4'),('24','6'),('25','1'),('25','2'),('25','3'),('25','5'),('25','6'),('25','7'),('26','2'),('26','5'),('26','6'),('26','7'),('27','1'),('27','3'),('27','4'),('27','5'),('27','6'),('28','1'),('28','2'),('28','3'),('28','4'),('28','5'),('28','6'),('28','7'),('29','1'),('29','4'),('29','5'),('29','6'),('30','3'),('30','5'),('30','6');
/*!40000 ALTER TABLE `report` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-09-30  1:28:37
