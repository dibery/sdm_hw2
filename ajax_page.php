<!-- Table Header -->

<table align=center style="width:50%" id="result">
<tr><td colspan=2 style="background-color:#777; color:#f8f8f8"><b>Orders</b></td></tr>
<tr><td style="width:40%; background-color:#777; color:#f8f8f8"><b>ID</b></td><td style="background-color:#777; color:#f8f8f8"><b>Customer</b></td></tr>

<!-- Table Content -->

<?php

if( !isset( $_GET[ "user" ] ) || $_GET[ "user" ] == "" )
	echo "<tr><td colspan=2>Query not specified.</td></tr>";

else
{
	$DB = new mysqli( "localhost", "root", "secure1234", "order_application" );

	if( $DB->connect_errno )
		die( "<tr><td colspan=2>Database connecting error</td></tr>" );

	else
	{
		$result = $DB->query( "select OrderID, Name from customer, orders where '" . $DB->real_escape_string( $_GET[ "user" ] ) . "' = Name and CustomerID = UserID order by abs( OrderID )" );

		if( $result->num_rows )
			while( $row = $result->fetch_assoc() )
				echo '<tr><td><a href="second_page.php?id=' . $row[ "OrderID" ] . '" target="_blank">' . $row[ "OrderID" ] . "</a></td><td>" . $row[ "Name" ] . "</td></tr>";
		else
			echo "<tr><td colspan=2>No Matched Orders</td></tr>";

		$result->close();
	}
	$DB->close();
}
?>

<!-- Table Footer -->

</table>
