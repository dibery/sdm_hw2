<!DOCTYPE html>
<html>

<head>
<title>SDM Homework #2 - Second Page</title>
<link rel="stylesheet" type="text/css" href="style.css" >
</head>

<body>

<!-- Result table (header part) -->

<table align=center style="width:50%">
<tr><td colspan=2 style="background-color:#777; color:#f8f8f8"><b>Orders</b></td></tr>

<!-- Result table (body part) -->

<?php
if( !isset( $_GET[ "id" ] ) )
	echo "<tr><td colspan=2>Order not found! (Order ID not specified.)</td></tr>";

else
{
	$DB = new mysqli( "localhost", "root", "secure1234", "order_application" );

	if( $DB->connect_errno )
		die( "<tr><td colspan=2>Database connecting error</td></tr>" );

	else
	{
		$query = $DB->real_escape_string( $_GET[ "id" ] );
		$cmd = "select Name, ItemName from customer, orders, report, item where report.OrderID = " . $query . " and orders.OrderID = " . $query . " and CustomerID = UserID and report.ItemID = item.ItemID";
		$result = $DB->query( $cmd );
		$first = true;

		if( $result->num_rows )
		{
			echo '<tr><td>ID</td><td>' . $query . '</td></tr>';
			while( $row = $result->fetch_assoc() )
			{
				if( $first )
				{
					$first = false;
					echo '<tr><td>customer</td><td>' . $row[ "Name" ] . '</td></tr>';
					echo '<tr><td>items</td><td>';
				}
				echo $row[ "ItemName" ] . '<br>';
			}
			echo '</td></tr>';
		}
		else
			echo "<tr><td colspan=2>Order not found!</td></tr>";
		$result->close();
	}
	$DB->close();
}
?>

<!-- Result table (footer part) -->

</table>

<!-- End of page body -->

</body>

</html>
